﻿using Log_In.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Log_In.Core.Interfaces
{
    public interface IPolicy
    {
        Task<IEnumerable<Policy>> GetPolicies();
    }
}
