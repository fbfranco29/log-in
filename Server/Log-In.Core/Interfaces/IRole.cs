﻿using Log_In.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Log_In.Core.Interfaces
{
    public interface IRole
    {
        Task<IEnumerable<Role>> GetRoles();
        Task<Role> GetRoleById(int id);

        Task<string> CreateRole(Role model);
        Task<string> UpdateRole(Role model);
        Task<string> DeleteRole(int id);
    }
}
