﻿using Log_In.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Log_In.Core.Interfaces
{
    public interface IUser
    {
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUserById(int id);

        Task<string> CreateUser(User model);
        Task<string> UpdateUser(User model);
        Task<string> DeleteUser(int id);
        Task<bool> EmailExist(string email, int id);
    }
}
