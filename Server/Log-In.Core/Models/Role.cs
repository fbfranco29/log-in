﻿using Log_In.Core.Models.Relationship_ManyToMany;
using System.Collections.Generic;

namespace Log_In.Core.Models
{
    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public ICollection<Role_Policy> Policies { get; set; }
    }
}
