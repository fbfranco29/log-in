﻿namespace Log_In.Core.Models.Relationship_ManyToMany
{
    public class Role_Policy
    {
        public int RoleId { get; set; }
        public Policy Policy { get; set; }

        public int PolicyId { get; set; }
        public Role Role { get; set; }
    }
}
