﻿using Log_In.Core.Models.Relationship_ManyToMany;
using System.Collections.Generic;

namespace Log_In.Core.Models
{
    public class Policy
    {
        public int PolicyId { get; set; }
        public string Description { get; set; }

        public ICollection<Role_Policy> Roles { get; set; }
    }
}
