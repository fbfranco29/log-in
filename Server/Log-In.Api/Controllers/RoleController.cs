﻿using Log_In.Core.Interfaces;
using Log_In.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Log_In.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        #region Property
        private readonly IRole _repo;
        private readonly IConfiguration _config;
        #endregion

        #region Constructor
        public RoleController(IRole repo, IConfiguration config)
        {
            _repo = repo;
            _config = config;
        }
        #endregion

        #region Controllers
        [HttpGet]
        [Route("LogIn_Api/Role")]
        public async Task<IEnumerable<Role>> Get()
        {
            return await _repo.GetRoles();
        }

        [HttpGet]
        [Route("LogIn_Api/Role/{id}")]
        public async Task<ActionResult<Role>> Get(int id)
        {
            var result = await _repo.GetRoleById(id);

            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        /// <summary>
        /// Crea un nuevo usuario.
        /// </summary>
        /// <param name="model"> Del model sugerido borrar las Propiedades:
        ///     Role
        ///     Companies
        ///     Groups
        /// </param>
        /// <returns>Mensage de confirmación(String)</returns>
        [HttpPost]
        [Route("LogIn_Api/Create/Role")]
        public async Task<IActionResult> Post([FromBody]Role model)
        {
            var result = await _repo.CreateRole(model);
            if (result.Equals("Se Guardó correctamente."))
            {
                return Ok(result);
            }
            return StatusCode(500, result);
        }

        [HttpPut]
        [Route("LogIn_Api/Update/Role/{id}")]
        public async Task<ActionResult> Put(int id, [FromBody]Role model)
        {
            if (id != model.RoleId)
            {
                return BadRequest();
            }

            var exist = await _repo.GetRoleById(id);
            if (exist == null)
            {
                return NotFound();
            }

            var result = await _repo.UpdateRole(model);
            if (result.Equals("Se Actualizó correctamente."))
            {
                return Ok(result);
            }
            return StatusCode(500, result);
        }

        [HttpDelete]
        [Route("LogIn_Api/Delete/Role/{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var exist = await _repo.GetRoleById(id);
            if (exist == null)
            {
                return NotFound();
            }

            var result = await _repo.DeleteRole(id);
            if (result.Equals("Se Eliminó correctamente."))
            {
                return Ok(result);
            }
            return StatusCode(500, result);
        }
        #endregion
    }
}