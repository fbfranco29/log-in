﻿using Log_In.Core.Interfaces;
using Log_In.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Log_In.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region Property
        private readonly IUser _repo;
        private readonly IConfiguration _config;
        #endregion

        #region Constructor
        public UserController(IUser repo, IConfiguration config)
        {
            _repo = repo;
            _config = config;
        }
        #endregion

        #region Controllers
        [HttpGet]
        [Route("LogIn_Api/User")]
        public async Task<IEnumerable<User>> Get()
        {
            return await _repo.GetUsers();
        }

        [HttpGet]
        [Route("LogIn_Api/User/{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            var result = await _repo.GetUserById(id);

            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        /// <summary>
        /// Crea un nuevo usuario.
        /// </summary>
        /// <param name="model"> Del model sugerido borrar las Propiedades:
        ///     Role
        ///     Companies
        ///     Groups
        /// </param>
        /// <returns>Mensage de confirmación(String)</returns>
        [HttpPost]
        [Route("LogIn_Api/Create/User")]
        public async Task<IActionResult> Post([FromBody]User model)
        {
            var result = await _repo.CreateUser(model);
            if (result.Equals("Se Guardó correctamente."))
            {
                return Ok(result);
            }
            return StatusCode(500, result);
        }

        [HttpPut]
        [Route("LogIn_Api/Update/User/{id}")]
        public async Task<ActionResult> Put(int id, [FromBody]User model)
        {
            if (id != model.UserId)
            {
                return BadRequest();
            }

            var exist = await _repo.GetUserById(id);
            if (exist == null)
            {
                return NotFound();
            }

            var result = await _repo.UpdateUser(model);
            if (result.Equals("Se Actualizó correctamente."))
            {
                return Ok(result);
            }
            return StatusCode(500, result);
        }

        [HttpDelete]
        [Route("LogIn_Api/Delete/User/{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var exist = await _repo.GetUserById(id);
            if (exist == null)
            {
                return NotFound();
            }

            var result = await _repo.DeleteUser(id);
            if (result.Equals("Se Eliminó correctamente."))
            {
                return Ok(result);
            }
            return StatusCode(500, result);
        }

        /// <summary>
        /// Método para validar la existencia de un email, para evitar duplicados.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="id"></param>
        /// <returns>True Si existe el email en DB y False en caso contrario</returns>
        [HttpGet]
        [Route("LogIn_Api/EmailValidate/{email}/{id}")]
        public async Task<ActionResult<bool>> EmailValidate(string email, int id)
        {
            var result = await _repo.EmailExist(email, id);
            return result;
        }
        #endregion
    }
}