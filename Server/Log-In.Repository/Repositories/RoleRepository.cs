﻿using Log_In.Core.Interfaces;
using Log_In.Core.Models;
using Log_In.Repository.Config;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Log_In.Repository.Repositories
{
    public class RoleRepository: IRole
    {
        #region Property
        private readonly LogIn_DbContext _context;
        #endregion

        #region Constructor
        public RoleRepository(LogIn_DbContext appDbContext)
        {
            _context = appDbContext;
        }
        public RoleRepository()
        {

        }
        #endregion

        #region Methods
        public async Task<string> CreateRole(Role model)
        {
            try
            {
                await _context.Role.AddAsync(model);
                await _context.SaveChangesAsync();
                return "Se Guardó correctamente.";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return ex.ToString();
            }
        }
        public async Task<string> DeleteRole(int id)
        {
            try
            {
                var remove = await GetRoleById(id);
                _context.Remove(remove);
                await _context.SaveChangesAsync();
                return "Se Eliminó correctamente.";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return ex.ToString();
            }
        }
        public async Task<Role> GetRoleById(int id)
        {
            try
            {
                return await _context.Role.Where(x => x.RoleId == id).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }
        public async Task<IEnumerable<Role>> GetRoles()
        {
            try
            {
                return await _context.Role.Include(x => x.Policies).ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }
        public async Task<string> UpdateRole(Role model)
        {
            try
            {
                _context.Update(model);
                await _context.SaveChangesAsync();
                return "Se Actualizó correctamente.";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return ex.ToString();
            }
        }
        #endregion
    }
}
