﻿using Log_In.Core.Interfaces;
using Log_In.Core.Models;
using Log_In.Repository.Config;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Log_In.Repository.Repositories
{
    public class UserRepository: IUser
    {
        #region Property
        private readonly LogIn_DbContext _context;
        #endregion

        #region Constructor
        public UserRepository(LogIn_DbContext appDbContext)
        {
            _context = appDbContext;
        }
        public UserRepository()
        {

        }
        #endregion

        #region Methods
        public async Task<string> CreateUser(User model)
        {
            try
            {
                await _context.User.AddAsync(model);
                await _context.SaveChangesAsync();
                return "Se Guardó correctamente.";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return ex.ToString();
            }
        }
        public async Task<string> DeleteUser(int id)
        {
            try
            {
                var remove = await GetUserById(id);
                _context.Remove(remove);
                await _context.SaveChangesAsync();
                return "Se Eliminó correctamente.";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return ex.ToString();
            }
        }
        public async Task<User> GetUserById(int id)
        {
            try
            {
                return await _context.User.Where(x => x.UserId == id).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }
        public async Task<IEnumerable<User>> GetUsers()
        {
            try
            {
                return await _context.User.Include(x => x.Role).ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }
        public async Task<string> UpdateUser(User model)
        {
            try
            {
                _context.Update(model);
                await _context.SaveChangesAsync();
                return "Se Actualizó correctamente.";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return ex.ToString();
            }
        }
        
        public async Task<bool> EmailExist(string email, int id)
        {
            try
            {
                var exist = await _context.User.Where(x => x.Email == email && x.UserId != id).ToListAsync();
                return exist.Any();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
        #endregion
    }
}
