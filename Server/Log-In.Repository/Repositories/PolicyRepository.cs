﻿using Log_In.Core.Interfaces;
using Log_In.Core.Models;
using Log_In.Repository.Config;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Log_In.Repository.Repositories
{
    public class PolicyRepository: IPolicy
    {
        #region Property
        private readonly LogIn_DbContext _context;
        #endregion

        #region Constructor
        public PolicyRepository(LogIn_DbContext appDbContext)
        {
            _context = appDbContext;
        }
        public PolicyRepository()
        {

        }
        #endregion

        #region Methods
        public async Task<IEnumerable<Policy>> GetPolicies()
        {
            try
            {
                return await _context.Policy.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }
        #endregion
    }
}
