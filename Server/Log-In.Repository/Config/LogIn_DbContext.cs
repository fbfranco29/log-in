﻿using Log_In.Core.Models;
using Log_In.Core.Models.Relationship_ManyToMany;
using Microsoft.EntityFrameworkCore;

namespace Log_In.Repository.Config
{
    public class LogIn_DbContext: DbContext
    {
        #region Constructor
        public LogIn_DbContext() { }
        public LogIn_DbContext(DbContextOptions options) : base(options) { }
        #endregion

        #region Properties
        public DbSet<User> User { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Policy> Policy { get; set; }
        public DbSet<Role_Policy> Role_Policy { get; set; }
        #endregion

        #region Methods
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            //Environment.SetEnvironmentVariable("SBS_SQL_CONNECTIONSTRING", @"Data Source=BISMARCK; Initial Catalog=SBS_DB; user=bfranco; password=5323182");
            //optionsBuilder.UseSqlServer(Environment.GetEnvironmentVariable("SBS_SQL_CONNECTIONSTRING"));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            #region Table User
            modelBuilder.Entity<User>().HasKey(x => x.UserId);
            modelBuilder.Entity<User>().Property(x => x.Name).HasMaxLength(30).IsRequired();
            modelBuilder.Entity<User>().Property(x => x.RoleId).IsRequired();
            modelBuilder.Entity<User>().Property(x => x.Email).HasMaxLength(30).IsRequired();
            modelBuilder.Entity<User>().HasIndex(x => x.Email).IsUnique();
            #endregion

            #region Table Role
            modelBuilder.Entity<Role>().HasKey(x => x.RoleId);
            modelBuilder.Entity<Role>().Property(x => x.RoleName).HasMaxLength(20).IsRequired();

            modelBuilder.Entity<Role>().HasData(new Role { RoleId = 1, RoleName = "Administrador" });
            modelBuilder.Entity<Role>().HasData(new Role { RoleId = 2, RoleName = "Invitado" });
            #endregion

            #region Table Policy
            modelBuilder.Entity<Policy>().HasKey(x => x.PolicyId);
            modelBuilder.Entity<Policy>().Property(x => x.Description).HasMaxLength(200).IsRequired();

            modelBuilder.Entity<Policy>().HasData(new Policy { PolicyId = 1, Description = "Agregar Usuario" });
            modelBuilder.Entity<Policy>().HasData(new Policy { PolicyId = 2, Description = "Editar Usuario" });
            modelBuilder.Entity<Policy>().HasData(new Policy { PolicyId = 3, Description = "Eliminar Usuario" });
            modelBuilder.Entity<Policy>().HasData(new Policy { PolicyId = 4, Description = "Agregar Rol" });
            modelBuilder.Entity<Policy>().HasData(new Policy { PolicyId = 5, Description = "Editar Rol" });
            modelBuilder.Entity<Policy>().HasData(new Policy { PolicyId = 6, Description = "Eliminar Rol" });
            #endregion

            #region Relationship Role-Policy
            modelBuilder.Entity<Role_Policy>().HasKey(x => new { x.RoleId, x.PolicyId });
            modelBuilder.Entity<Role_Policy>().HasOne(x => x.Role).WithMany(m => m.Policies).HasForeignKey(x => x.RoleId);
            modelBuilder.Entity<Role_Policy>().HasOne(x => x.Policy).WithMany(m => m.Roles).HasForeignKey(x => x.PolicyId);
            #endregion
        }
        #endregion
    }
}
